export const statusList = [
  "En attente",
  "En cours",
  "Fini",
  "Bloque",
  "Abandonne"
];

export const categoryList = ["Technique", "Design", "Redactionnel"];
