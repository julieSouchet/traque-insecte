/** "enCours" => "En cours" */
export const fromCamelCase = str => {
  let res = "";
  for (let index = 0; index < str.length; index++) {
    const char = str[index];
    if (index === 0) res += " " + char.toUpperCase();
    else if (char.match(/[A-Z]/)) res += " " + char.toLowerCase();
    else res += char;
  }
  return res.trim();
};

/** "En cours" => "enCours"  */
export const toCamelCase = str => {
  let res = "";
  for (let index = 0; index < str.length; index++) {
    const char = str[index];
    if (index === 0) res += " " + char.toLowerCase();
    else if (char.match(/\s/)) res += str[++index].toUpperCase();
    else res += char;
  }
  return res.trim();
};

/** "azertyuiop" => "A" */
export const initial = str => str[0].toUpperCase();
