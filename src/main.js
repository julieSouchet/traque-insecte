import Vue from "vue";
import App from "./App.vue";
import router from "./router";
// import "tailwindcss/tailwind.css";

import { fromCamelCase, toCamelCase, initial } from "@/utils/string";
import "./index.css";

// VUE JS
Vue.config.productionTip = false;

Vue.filter("fromCamelCase", str => fromCamelCase(str));
Vue.filter("toCamelCase", str => toCamelCase(str));
Vue.filter("initial", str => initial(str));

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
