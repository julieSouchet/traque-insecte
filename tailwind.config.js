module.exports = {
  purge: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      height: {
        wide: "90vh",
        half: "50vh",
        fit: "fit-content"
      },
      width: {
        wide: "90vw",
        half: "50vw",
        fit: "fit-content"
      },
      minHeight: {
        "0": "0",
        "1/4": "25%",
        "1/2": "50%",
        "3/4": "75%",
        wide: "90vh",
        half: "50vh",
        full: "100%"
      },
      minWidth: {
        "0": "0",
        "1/4": "25%",
        "1/2": "50%",
        "3/4": "75%",
        wide: "90vw",
        half: "50vw",
        full: "100%"
      },
      maxHeight: {
        "0": "0",
        "1/4": "25%",
        "1/2": "50%",
        "3/4": "75%",
        wide: "90vh",
        half: "50vh",
        full: "100%"
      },
      maxWidth: {
        "0": "0",
        "1/4": "25%",
        "1/2": "50%",
        "3/4": "75%",
        wide: "90vw",
        half: "50vw",
        full: "100%"
      },
      colors: {
        primary: "#95B8D1",
        link: "#EDAFB8",
        linkDark: "#CB8D96",
        light: "#faf9f9",
        dark: "#333333",
        secondary: "#D8DDB5",
        enAttente: "#ffd166",
        enCours: "#06d6a0",
        bloque: "#ef476f",
        fini: "#118ab2",
        abandonne: "#073b4c",
        design: "#4ECDC4",
        redactionnel: "#FFE66D",
        technique: "#FF6B6B"
      },
      // backgroundColor: ["active"],
      shadow: ["active"]
      // color: ["active"]
    }
  },
  variants: {
    extend: {}
  },
  plugins: []
};
