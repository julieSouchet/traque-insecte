# Bug Tracker

## Objectif
L’objectif est de créer une application permettant d’avoir un suivi des bugs sur différents projets. Utile pour faire du resettage par exemple.


## Fonctionnalités

On aura la possibilité de créer des projets avec :

* un nom,
* une description
* (peut être une image).

Ensuite, dans chacun des projets, on pourra ajouter des tickets.
Vous aurez un espace pour lister les membres de l’équipe

Chaque ticket devra comporter les fonctionnalités suivantes : 

* un utilisateur qui a créé le ticket
* un utilisateur assigné au ticket
* des étiquettes indiquant clairement le statut
* une date de début
* une date de fin (avec indication si le temps est dépassé)
* un titre
* une description du bug
* la catégorie du bug (technique / design / Rédactionnel etc…)
* la possibilité de joindre des captures d’écran ou des fichiers PDF


## BONUS
* Chaque “carte” pourra avoir sa couleur qui change en fonction de différents paramètres.
Par ex si la date limite est dépassée, la carte passe en rouge et une étiquette est ajoutée.
Ou par ex la carte peut avoir un code couleur en fonction de la nature du bug :

 * technique
 * Bug Front-end
 * Faute d’orthographe
 * etc.

* Chaque utilisateur de l’équipe doit avoir une fiche dédiée :
 * qui liste ses compétences
 * qui affiche sa bio (avatar et description)
 * qui affiche son statut : disponible ou non. Précisons que le statut peut être lié au nombre de cartes sur lequel il est assigné !


## Technos

* Le projet sera en Vue JS pour le front.
* Tout le backend sera fait avec Firebase. Firebase va gérer :
   * la base de données,
   * l’authentification,
   * l’upload d’images
   * et la mise en ligne.

* Il faudra mettre en ligne le projet sur Firebase
* Il faudra utiliser un framework CSS sans JS
* L’application n’a pas besoin d'être responsive : cible desktop

Projet à faire en binôme libre.
